# Fixing permissions for external /data volumes
mkdir -p /data/blobstorage /data/cache /data/filestorage /data/instance /data/log /data/zeoserver
mkdir -p /plone/instance/src

# Initializing from environment variables
python /docker-initialize.py

buildout -Nc custom.cfg

# find /data  -not -user plone -exec chown plone:plone {} \+
# find /plone -not -user plone -exec chown plone:plone {} \+
