#!/bin/bash
set -e
cat << _EOF > /tmp/zodbpack.cfg
<relstorage source>
  blob-dir /var/backup/cache
  <postgresql>
    dsn host=${DB_HOST} user=${DB_USER} password='${DB_PASS}' dbname='${DB_NAME}'
  </postgresql>
</relstorage>
_EOF
chmod 0400 /tmp/zodbpack.cfg
/plone/instance/bin/zodbpack /tmp/zodbpack.cfg
rm -f /tmp/zodbpack.cfg


