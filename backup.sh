#!/bin/bash
set -e
if [[ ! -e /var/backup ]]; then echo "missing /var/backup mountpoint"; exit -1; fi

cat << _EOF > /tmp/to_filestorage.cfg
<filestorage destination>
  path /var/backup/filestorage/Data.fs
  blob-dir /var/backup/blobstorage
</filestorage>

<relstorage source>
  blob-dir /var/backup/cache
  <postgresql>
    dsn host=${DB_HOST} user=${DB_USER} password='${DB_PASS}' dbname='${DB_NAME}'
  </postgresql>
</relstorage>
_EOF
chmod 0400 /tmp/to_filestorage.cfg
test -e /var/backup/filestorage || mkdir /var/backup/filestorage
test -e /var/backup/blobstorage || mkdir /var/backup/blobstorage
test -e /var/backup/cache || mkdir /var/backup/cache
/plone/instance/bin/zodbconvert --incremental /tmp/to_filestorage.cfg
rm -rf /var/backup/cache
rm -f /tmp/to_filestorage.cfg

