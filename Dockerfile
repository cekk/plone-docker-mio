FROM redturtletech/plone:5.2.4-debian

#COPY versions buildout.sh extends.cfg ./
#COPY docker-entrypoint.sh backup.sh pack.sh ./
COPY versions ./versions
COPY buildout.sh ./buildout.sh
COPY extends.cfg ./extends.cfg
COPY docker-entrypoint.sh /docker-entrypoint.sh
COPY backup.sh /backup.sh
COPY pack.sh /pack.sh


ENV ADDONS "giovazoom.policy rer.block.iframembed"
ENV SOURCES "giovazoom.policy = git https://agamar:x3vpXy3fTZV9HWs7DyzQ@gitlab.com/redturtle/giovazoom/giovazoom.policy.git, rer.block.iframembed = git https://github.com/RegioneER/rer.block.iframembed.git"

ENV BUILDOUT_EXTENDS="extends.cfg versions/base.cfg"
ENV RELSTORAGE_ADAPTER_OPTIONS="type postgresql, host $(DB_HOST), dbname $(DB_NAME), user $(DB_USER), password $(DB_PASS)"
ENV RELSTORAGE_CACHE_LOCAL_DIR=/data/cache
ENV RELSTORAGE_BLOB_CACHE_SIZE=1gb

# TEMP

RUN rm /plone/instance/var/filestorage \
    && rm /plone/instance/var/blobstorage \
    && chgrp -R 0 /data && chmod -R g=u /data \
    && chgrp -R 0 /plone && chmod -R g=u /plone

RUN ./buildout.sh

USER 1001

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["start"]
